//
//  MenuBtn.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/1/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit

class MenuBtn: UIButton {
    
    override func awakeFromNib() {
        layer.cornerRadius = 10
        clipsToBounds = true
    }

}
