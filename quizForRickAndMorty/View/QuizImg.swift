//
//  QuizImg.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/2/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit

class QuizImg: UIImageView {
    
    override func awakeFromNib() {
        layer.cornerRadius = 5.0
        clipsToBounds = true
        layer.borderWidth = 2.0
        layer.borderColor = #colorLiteral(red: 0.168627451, green: 0.6862745098, blue: 0.137254902, alpha: 1)
    }
}
