//
//  PurchaseManager.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/14/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation
import StoreKit

class PurchaseManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    static let instance = PurchaseManager()
    
    let IAP_plus300Coins = "waygem.300coins"
    let IAP_plus750Coins = "waygem.750coins"
    let IAP_plus2000Coins = "waygem.2000coins"
    let IAP_plus5000Coins = "waygem.5000coins"
    let IAP_removeAds = "waygem.noAds"
    var products = [SKProduct]()
    var product = SKProduct()
    var delegate: PurchaseManagerDelegate?
    
    func fetchProducts() {
        if(SKPaymentQueue.canMakePayments()) {
            let productIds = NSSet(objects: IAP_plus300Coins, IAP_plus750Coins, IAP_plus2000Coins, IAP_plus5000Coins, IAP_removeAds) as! Set<String>
            let productsRequest: SKProductsRequest = SKProductsRequest(productIdentifiers: productIds)
            productsRequest.delegate = self
            productsRequest.start()
        }
    }
    
    func buyProduct() {
        let pay = SKPayment(product: product)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(pay as SKPayment)
    }
    
    func selectProduct(productNum: Int) {
        var productID = ""
        switch productNum {
        case 1:
            productID = IAP_plus300Coins
        case 2:
            productID = IAP_plus750Coins
        case 3:
            productID = IAP_plus2000Coins
        case 4:
            productID = IAP_plus5000Coins
        case 5:
            productID = IAP_removeAds
        default:
            productID = ""
        }
        for p in products {
            if p.productIdentifier == productID {
                product = p
                buyProduct()
            }
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let myProducts = response.products
        for product in myProducts {
            products.append(product)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                let productID = product.productIdentifier
                switch productID {
                case IAP_plus300Coins:
                    delegate?.addCoins(coins: 300)
                case IAP_plus750Coins:
                    delegate?.addCoins(coins: 750)
                case IAP_plus2000Coins:
                    delegate?.addCoins(coins: 2000)
                case IAP_plus5000Coins:
                    delegate?.addCoins(coins: 5000)
                case IAP_removeAds:
                    UserDefaults.standard.set(true, forKey: "noAds")
                default:
                    print("IAP not found")
                }
                queue.finishTransaction(transaction)
            case .failed:
                queue.finishTransaction(transaction)
                break
            default:
                break
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("transactions restored")
        for transaction in queue.transactions {
            let productID = transaction.payment.productIdentifier as String
            switch productID {
            case IAP_removeAds:
                UserDefaults.standard.set(true, forKey: "noAds")
                print("restored")
            default:
                print("IAP not found")
            }
        }
    }
    
    func restorePurchases() {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

protocol PurchaseManagerDelegate {
    func addCoins(coins: Int)
}
