//
//  Letters.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/6/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation

class Letters {
    let firstLevelAnswers = ["Rick", "Morty", "Jerry", "Birdperson", "Beth", "Mr Meeseeks", "Summer", "Squanchy", "Abradolf Lincler", "Pickle Rick"]
    let secondLevelAnswers = ["Snuffles", "Tiny Rick", "Supernova", "Crocubot", "Cop Morty", "Armagheadon", "Fart", "Million Ants", "Tammy", "Hemorrhage"]
    let thirdLevelAnswers = ["Noob Noob", "Butter Robot", "Vance Maximus", "Scary Terry", "Cop Rick", "Worldender", "Evil Morty", "The President", "Unity", "Lil Bits"]
    let fourthLevelAnswers = ["Jessica", "Cromulons", "Dr Wong", "Ice T", "General Nathan", "Annie", "Blim Blam", "Jaguar", "Baby Legs", "Gromflomite"]
    let fifthLevelAnswers = ["Doofus Rick", "Scroopy", "Commander Rick", "Lawyer Morty", "Morty Junior", "Mrs Sanchez", "Jerry Mytholog", "Mr Goldenfold", "Lil B", "Davin"]
    let sixthLevelAnswers = ["Cool Rick", "Armothy", "Slaveowner", "Keara", "Brad", "Shnoopy Bloopers", "Ethan", "Thomas Lipkip", "Alan Rails", "Cornvelious"]
    
    func getAnswersArray(level: Int) -> [String] {
        switch level {
        case 1:
            return firstLevelAnswers
        case 2:
            return secondLevelAnswers
        case 3:
            return thirdLevelAnswers
        case 4:
            return fourthLevelAnswers
        case 5:
            return fifthLevelAnswers
        case 6:
            return sixthLevelAnswers
        default:
            return firstLevelAnswers
        }
    }
    
    func countLettersInAnswer(level: Int, question: Int) -> (Int, Int) {
        var answer = ""
        answer = getAnswersArray(level: level)[question - 1]
        let components = answer.components(separatedBy: .whitespacesAndNewlines)
        let words = components.filter { !$0.isEmpty }
        if words.count == 1 {
            return (words[0].count, 0)
        } else if words.count >= 2 {
            return (words[0].count, words[1].count)
        } else {
            return (0, 0)
        }
    }
    
    func getLetters(level: Int, question: Int) -> ([String], [String]) {
        var answerLetters = [String]()
        let alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        let answer = getAnswersArray(level: level)[question-1].components(separatedBy: .whitespaces).joined()
        for l in 0...answer.count-1 {
            answerLetters.append(String(answer[l]))
        }
        var shuffledAlphabet = alphabet.shuffled()
        var wrongLetters = [String]()
        for i in 0...15 - answer.count {
            answerLetters.append(shuffledAlphabet[i])
            wrongLetters.append(shuffledAlphabet[i])
        }
        return (answerLetters.shuffled(), wrongLetters)
    }
    
    func getAnswerWord(level: Int, question: Int) -> String {
        let answer = getAnswersArray(level: level)[question-1].components(separatedBy: .whitespaces).joined().uppercased()
        return answer
    }
    
    
}

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
}

extension MutableCollection {
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}
