//
//  Sounds.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/27/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation

class Sounds {
    
    static let instance = Sounds()
    
    let btnURL = Bundle.main.url(forResource: "btn", withExtension: "wav")
    var btnSound: Sound?
    
    let letterURL = Bundle.main.url(forResource: "letter", withExtension: "wav")
    var letterSound: Sound?
    
    let backURL = Bundle.main.url(forResource: "back", withExtension: "wav")
    var backSound: Sound?
    
    func changeVolume() {
        btnSound?.volume = 0.1
        letterSound?.volume = 0.3
        backSound?.volume = 0.1
    }
}
