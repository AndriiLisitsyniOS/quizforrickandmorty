//
//  Score.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/7/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation

class Score {
    
    static let instance = Score()
        
    func saveScore(score: Int) {
        UserDefaults.standard.set(score, forKey: "score")
    }
    
    func getScore() -> Int {
        return UserDefaults.standard.integer(forKey: "score")
    }
}
