//
//  LevelsVC.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/2/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit
import GoogleMobileAds

class LevelsVC: UIViewController {
    
    @IBOutlet weak var btnsStack: UIStackView!
    @IBOutlet var lvlProgressLbls: [UILabel]!
    @IBOutlet var lockImgs: [UIImageView]!
    @IBOutlet var lockCoinsImgs: [UIImageView]!
    @IBOutlet var lockCoinsLbls: [UILabel]!
    @IBOutlet var levelBtns: [MenuBtn]!
    @IBOutlet var noCoinsBtn: [UIButton]!
    @IBOutlet weak var coinsLbl: UILabel!
    
    var levelQuestionDictionary = ["1":0, "2":0, "3":0, "4":0, "5":0, "6":0] as [String:Any]
    var openLevelsArray = ["open", "locked", "locked", "locked", "locked", "locked"]
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if GoogleAds.instance.show {
            GoogleAds.instance.appearInterstitial(vc: self)
            GoogleAds.instance.createAndLoadInterstitial()
            GoogleAds.instance.show = false
        }
        
        score = Score.instance.getScore()
        coinsLbl.text = "\(score)"
        
        if UserDefaults.standard.array(forKey: "openLevelsArray") != nil {
            openLevelsArray = UserDefaults.standard.array(forKey: "openLevelsArray") as! [String]
        }
        if UserDefaults.standard.dictionary(forKey: "levelQuestionDictionary") != nil {
            levelQuestionDictionary = UserDefaults.standard.dictionary(forKey: "levelQuestionDictionary")!
        }
        
        for n in 1...5 {
            if openLevelsArray[n] == "open" {
                lockImgs[n-1].isHidden = true
                lockCoinsImgs[n-1].isHidden = true
                lockCoinsLbls[n-1].isHidden = true
                lvlProgressLbls[n].isHidden = false
            }
        }
        
        for p in 0...5 {
            lvlProgressLbls[p].text = "\(levelQuestionDictionary[String(p+1)]!)/10"
            if lvlProgressLbls[p].text == "10/10" {
                levelBtns[p].isEnabled = false
            }
        }
        
        for x in 0...4 {
            if openLevelsArray[x+1] == "locked" && score < 50 {
                noCoinsBtn[x].isHidden = false
                noCoinsBtn[x].addTarget(self, action: #selector(noCoinsBtnAction), for: .touchUpInside)
            } else {
                noCoinsBtn[x].isHidden = true
            }
        }
    }
    
    @objc func noCoinsBtnAction() {
        Sounds.instance.btnSound?.play()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "levelOne" {
            let vc = segue.destination as! QuizVC
            vc.level = 1
        } else if segue.identifier == "levelTwo" {
            let vc = segue.destination as! QuizVC
            vc.level = 2
        } else if segue.identifier == "levelThree" {
            let vc = segue.destination as! QuizVC
            vc.level = 3
        } else if segue.identifier == "levelFour" {
            let vc = segue.destination as! QuizVC
            vc.level = 4
        } else if segue.identifier == "levelFive" {
            let vc = segue.destination as! QuizVC
            vc.level = 5
        } else if segue.identifier == "levelSix" {
            let vc = segue.destination as! QuizVC
            vc.level = 6
        }
    }
    
    func levelBtnPressed(level: Int) {
        Sounds.instance.btnSound?.play()
        if openLevelsArray[level-1] == "locked" {
            score -= 50
            Score.instance.saveScore(score: score)
            coinsLbl.text = String(score)
            openLevelsArray[level-1] = "open"
            UserDefaults.standard.set(openLevelsArray, forKey: "openLevelsArray")
            lvlProgressLbls[level-1].isHidden = false
            lockImgs[level-2].isHidden = true
            lockCoinsImgs[level-2].isHidden = true
            lockCoinsLbls[level-2].isHidden = true
        }
    }
    @IBAction func firstLevelBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
    }
    
    @IBAction func levelsBtnPressed(_ sender: UIButton) {
        levelBtnPressed(level: sender.tag)
    }
    
    @IBAction func storeBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        Sounds.instance.backSound?.play()
        dismiss(animated: false, completion: nil)
    }
}
