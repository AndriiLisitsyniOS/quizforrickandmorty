//
//  StoreVC.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/2/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit
import GoogleMobileAds

class StoreVC: UIViewController, PurchaseManagerDelegate {

    @IBOutlet weak var coinsLbl: UILabel!
    
    var score = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PurchaseManager.instance.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        score = Score.instance.getScore()
        coinsLbl.text = String(score)
    }

    @IBAction func buyCoinsBtnPressed(_ sender: UIButton) {
        Sounds.instance.btnSound?.play()
        PurchaseManager.instance.selectProduct(productNum: sender.tag)
    }
    
    @IBAction func restoreBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
        PurchaseManager.instance.restorePurchases()
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        Sounds.instance.backSound?.play()
        dismiss(animated: false, completion: nil)
    }
    
    //PurchaseManagerDelegate:
    
    func addCoins(coins: Int) {
        score += coins
        Score.instance.saveScore(score: score)
        coinsLbl.text = "\(score)"
        
    }
}
