//
//  ViewController.swift
//  quizForRickAndMorty
//
//  Created by andrey on 6/30/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ViewController: UIViewController {

    @IBOutlet weak var coinsLbl: UILabel!
    var numLaunch = UserDefaults.standard.integer(forKey: "numLaunch") + 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if numLaunch == 1 {
            UserDefaults.standard.set(50, forKey: "score")
        }
        UserDefaults.standard.set(numLaunch, forKey: "numLaunch")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        coinsLbl.text = "\(Score.instance.getScore())"
    }
    
    @IBAction func btnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
    }
}

