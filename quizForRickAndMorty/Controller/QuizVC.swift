//
//  QuizVC.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/2/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit
import GoogleMobileAds

class QuizVC: UIViewController, GADRewardBasedVideoAdDelegate, GADInterstitialDelegate {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var levelNumberLbl: UILabel!
    @IBOutlet weak var letterBtnsView: UIView!
    @IBOutlet weak var titleBtnsView: UIView!
    @IBOutlet weak var correctAnswerView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var coinsLbl: UILabel!
    @IBOutlet var hintBtns: [UIButton]!
    @IBOutlet weak var storeBtn: UIButton!
    
    var letterBtns = Array(repeating: UIButton(), count: 16)
    var wrongLetters = [String]()
    var firstLineTitleBtns = [UIButton]()
    var secondLineTitleBtns = [UIButton]()
    var level = 1
    var question = 1
    var firstLineTitleLength = 0
    var secondLineTitleLength = 0
    var screenWidth = CGFloat(0)
    var titleBtnSpace = CGFloat(0)
    var titleBtnSide = CGFloat(0)
    var letterBtnSpace = CGFloat(0)
    var letterBtnSide = CGFloat(0)
    var levelQuestionDictionary = ["1":0, "2":0, "3":0, "4":0, "5":0, "6":0] as [String:Any]
    var openLevelsArray = ["open", "locked", "locked", "locked", "locked", "locked"]
    var score = 0
    var iPhone = true
    var iPhoneBannerHeightCn = NSLayoutConstraint()
    var interstitialDismissed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for h in hintBtns {
            h.layer.cornerRadius = 5.0
        }
        levelNumberLbl.text = "LEVEL \(level)"
        
        if UserDefaults.standard.dictionary(forKey: "levelQuestionDictionary") != nil {
            levelQuestionDictionary = UserDefaults.standard.dictionary(forKey: "levelQuestionDictionary")!
        }
        question = levelQuestionDictionary[String(level)] as! Int + 1
        if UserDefaults.standard.dictionary(forKey: "openLevelsArray") != nil {
            openLevelsArray = UserDefaults.standard.array(forKey: "openLevelsArray") as! [String]
        }
        
        imgView.image = UIImage(named: "\((level-1) * 10 + question)")
        screenWidth = view.frame.width
        letterBtnSpace = screenWidth/60
        letterBtnSide = (screenWidth - letterBtnSpace*9)/8
        titleBtnSpace = letterBtnSpace/1.5
        titleBtnSide = letterBtnSide/1.35
        letterBtnsView.heightAnchor.constraint(equalToConstant: 2*letterBtnSide + 3*letterBtnSpace).isActive = true
        titleBtnsView.heightAnchor.constraint(equalToConstant: 2*titleBtnSide + 3*titleBtnSpace).isActive = true
        let letters = Letters()
        let(answerLetters, wrongAnswers) = letters.getLetters(level: level, question: question)
        wrongLetters = wrongAnswers.shuffled()
        for b in 0...15 {
            if b < 8 {
                letterBtns[b] = UIButton(frame: CGRect(x: letterBtnSpace + (letterBtnSide + letterBtnSpace)*CGFloat(b), y: letterBtnSpace, width: letterBtnSide, height: letterBtnSide))
            } else {
                letterBtns[b] = UIButton(frame: CGRect(x: letterBtnSpace + (letterBtnSide + letterBtnSpace)*CGFloat(b-8), y: 2*letterBtnSpace + letterBtnSide, width: letterBtnSide, height: letterBtnSide))
            }
            letterBtns[b].layer.cornerRadius = 5
            letterBtns[b].clipsToBounds = true
            letterBtns[b].backgroundColor = #colorLiteral(red: 0.168627451, green: 0.6862745098, blue: 0.137254902, alpha: 1)
            letterBtns[b].tag = b + 1
            letterBtns[b].titleLabel?.font = UIFont(name: "Helvetica Neue", size: letterBtnSide/2)
            letterBtns[b].setTitle(answerLetters[b].uppercased(), for: .normal)
            letterBtns[b].addTarget(self, action: #selector(letterBtnPressed(sender:)), for: .touchUpInside)
            letterBtnsView.addSubview(letterBtns[b])
        }
        addTitleBtns()
        
        let infoViewTopCn = NSLayoutConstraint(item: infoView, attribute: .top, relatedBy: .equal, toItem: titleBtnsView, attribute: .bottom, multiplier: 1, constant: 10)
        view.addConstraint(infoViewTopCn)
        infoView.layer.cornerRadius = 8
        
        loadAndRequestRewardedVideo()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        score = Score.instance.getScore()
        coinsLbl.text = String(score)
    }
    
    func addTitleBtns() {
        let letters = Letters()
        let (firstWordCount, secondWordCount) = letters.countLettersInAnswer(level: level, question: question)
        firstLineTitleLength = firstWordCount
        secondLineTitleLength = secondWordCount
        
        firstLineTitleBtns = Array(repeating: UIButton(), count: firstLineTitleLength)
        
        for a in 0...firstLineTitleLength-1 {
            let answerBtnX = (screenWidth - (titleBtnSide + titleBtnSpace) * CGFloat(firstLineTitleLength) + titleBtnSpace)/2
            firstLineTitleBtns[a] = UIButton(frame: CGRect(x: answerBtnX + (titleBtnSide + titleBtnSpace) * CGFloat(a), y: titleBtnSpace, width: titleBtnSide, height: titleBtnSide))
        }
        secondLineTitleBtns = Array(repeating: UIButton(), count: secondLineTitleLength)
        if secondLineTitleLength != 0 {
            for a in 0...secondLineTitleLength-1 {
                 let answerBtnX = (screenWidth - (titleBtnSide + titleBtnSpace) * CGFloat(secondLineTitleLength) + titleBtnSpace)/2
                secondLineTitleBtns[a] = UIButton(frame: CGRect(x: answerBtnX + (titleBtnSide + titleBtnSpace) * CGFloat(a), y: 2*titleBtnSpace + titleBtnSide, width: titleBtnSide, height: titleBtnSide))
            }
        }
        let titleBtns = firstLineTitleBtns + secondLineTitleBtns
        for b in titleBtns {
            b.layer.cornerRadius = 5
            b.clipsToBounds = true
            b.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            b.addTarget(self, action: #selector(titleBtnPressed(sender:)), for: .touchUpInside)
            b.titleLabel?.font = UIFont(name: "Helvetica Neue", size: titleBtnSide/1.75)
            titleBtnsView.addSubview(b)
        }
    }
    
    @objc func letterBtnPressed(sender: UIButton) {
        let titleBtns = firstLineTitleBtns + secondLineTitleBtns
        for b in titleBtns {
            if b.titleLabel?.text == nil || b.titleLabel?.text == "" {
                Sounds.instance.letterSound?.play()
                b.setTitle(sender.titleLabel?.text, for: .normal)
                b.tag = sender.tag
                sender.isHidden = true
                checkAnswer()
                break
            }
        }
    }
    
    @objc func titleBtnPressed(sender: UIButton) {
        if sender.titleLabel?.text != "" && sender.titleLabel?.text != nil {
            Sounds.instance.letterSound?.play()
            sender.titleLabel?.text = ""
            sender.setTitle("", for: .normal)
            for b in letterBtns {
                if b.tag == sender.tag {
                    b.isHidden = false
                    sender.tag = 0
                }
            }
        }
    }
    
    func checkAnswer() {
        let titleBtns = firstLineTitleBtns + secondLineTitleBtns
        var answer = ""
        var stop = false
        
        for b in titleBtns {
            if b.titleLabel?.text != nil && b.titleLabel?.text != "" {
                answer = answer + (b.titleLabel?.text)!
            } else {
                stop = true
                break
            }
        }
        guard stop == false else { return }
        
        let letters = Letters()
        if answer.uppercased() == letters.getAnswersArray(level: level)[question-1].components(separatedBy: .whitespaces).joined().uppercased() {
            infoView.backgroundColor = #colorLiteral(red: 0.168627451, green: 0.6862745098, blue: 0.137254902, alpha: 1)
            correctAnswerView.isHidden = false
            infoView.isHidden = false
            if question == 10 {
                infoLbl.text = "LEVEL COMPLETE!"
                Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(levelComplete), userInfo: nil, repeats: false)
            } else {
                infoLbl.text = "CORRECT!!"
                Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(correctAnswer), userInfo: nil, repeats: false)
            }
            score += 5
            Score.instance.saveScore(score: score)
            coinsLbl.text = String(score)
        } else {
            infoLbl.text = "WRONG ANSWER"
            infoView.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.4784313725, blue: 0.07450980392, alpha: 1)
            infoView.isHidden = false
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(wrongAnswer), userInfo: nil, repeats: false)
        }
    }
    
    @objc func levelComplete() {
        levelQuestionDictionary.updateValue(question, forKey: String(level))
        UserDefaults.standard.set(levelQuestionDictionary, forKey: "levelQuestionDictionary")
        GoogleAds.instance.page += 1
        GoogleAds.instance.show = true
        dismiss(animated: true, completion: nil)
    }
    
    @objc func correctAnswer() {
        correctAnswerView.isHidden = true
        infoView.isHidden = true
        levelQuestionDictionary.updateValue(question, forKey: String(level))
        UserDefaults.standard.set(levelQuestionDictionary, forKey: "levelQuestionDictionary")
        question += 1
        imgView.image = UIImage(named: "\((level-1) * 10 + question)")
        let titleBtns = firstLineTitleBtns + secondLineTitleBtns
        for b in titleBtns {
            b.removeFromSuperview()
        }
        addTitleBtns()
        let letters = Letters()
        let (answerLetters, wrongAnswers) = letters.getLetters(level: level, question: question)
        wrongLetters = wrongAnswers.shuffled()
        var x = 0
        for b in letterBtns {
            b.isHidden = false
            b.setTitle(answerLetters[x].uppercased(), for: .normal)
            x += 1
        }
        GoogleAds.instance.page += 1
        if GoogleAds.instance.page == 1 {
            if #available(iOS 10.3, *){
                let appDelegate = AppDelegate()
                appDelegate.requestReview()
            } else if UserDefaults.standard.bool(forKey: "rated") != true {
                showAskReviewAlert()
            }
        } else if GoogleAds.instance.page % 2 == 0 {
            GoogleAds.instance.appearInterstitial(vc: self)
            GoogleAds.instance.createAndLoadInterstitial()
        }
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        interstitialDismissed = true
    }

    
    func showAskReviewAlert() {
        let alert = UIAlertController(title: "Rate Us\n", message:"If you enjoy this app, \nplease support us.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Rate App", style: .cancel) { _ in
            UIApplication.shared.open(URL(string : "itms-apps://itunes.apple.com/app/id1410705663")!, options: [:], completionHandler: nil)
            UserDefaults.standard.set(true, forKey: "rated")
        })
        alert.addAction(UIAlertAction(title: "Not now", style: .default) { _ in
        })
        self.present(alert, animated: true){}
    }
    
    @objc func wrongAnswer() {
        infoView.isHidden = true
    }
    
    @IBAction func delLetterBtnPressed(_ sender: Any) {
        if Score.instance.getScore() >= 30 {
        for w in wrongLetters {
            for b in letterBtns {
                if w == b.titleLabel?.text && b.isHidden == false{
                    b.isHidden = true
                    wrongLetters.remove(at: wrongLetters.index(of: w)!)
                    letterRemoved()
                    return
                }
            }
        }
        let titleBtns = firstLineTitleBtns + secondLineTitleBtns
        let letters = Letters()
        let answer = letters.getAnswerWord(level: level, question: question)
        for w in wrongLetters {
            for t in titleBtns {
                if w == t.titleLabel?.text && t.isEnabled == true && t.titleLabel?.text != String(answer[titleBtns.index(of: t)!]) {
                    t.titleLabel?.text = ""
                    t.setTitle("", for: .normal)
                    t.tag = 0
                    wrongLetters.remove(at: wrongLetters.index(of: w)!)
                    letterRemoved()
                    return
                }
            }
        }
        } else {
            storeBtn.sendActions(for: .touchUpInside)
        }
    }
    
    func letterRemoved() {
        Sounds.instance.backSound?.play()
        score -= 30
        coinsLbl.text = "\(score)"
        Score.instance.saveScore(score: score)
    }
    
    @IBAction func openLetterBtnPressed(_ sender: Any) {
        if Score.instance.getScore() >= 60 {
        let titleBtns = firstLineTitleBtns + secondLineTitleBtns
        let letters = Letters()
        let answer = letters.getAnswerWord(level: level, question: question)
        for t in titleBtns {
            if t.titleLabel?.text == nil || t.titleLabel?.text == "" {
                t.setTitle(String(answer[titleBtns.index(of: t)!]), for: .normal)
                t.titleLabel?.text = String(answer[titleBtns.index(of: t)!])
                t.setTitleColor(#colorLiteral(red: 0.168627451, green: 0.6862745098, blue: 0.137254902, alpha: 1), for: .normal)
                t.isEnabled = false
                var x = 0
                for b in letterBtns {
                    if b.titleLabel?.text == t.titleLabel?.text && b.isHidden == false {
                        b.isHidden = true
                        break
                    } else {
                        x += 1
                    }
                }
                if x == letterBtns.count {
                    for titleBtn in titleBtns {
                        if titleBtn.isEnabled == true && titleBtn.titleLabel?.text == t.titleLabel?.text && titleBtn.titleLabel?.text != String(answer[titleBtns.index(of: titleBtn)!]) {
                            titleBtn.titleLabel?.text = ""
                            titleBtn.setTitle("", for: .normal)
                            titleBtn.tag = 0
                            break
                        }
                    }
                }
                Sounds.instance.btnSound?.play()
                checkAnswer()
                score -= 60
                coinsLbl.text = "\(score)"
                Score.instance.saveScore(score: score)
                break
            }
        }
        } else {
            storeBtn.sendActions(for: .touchUpInside)
        }
    }
    
    @IBAction func freeCoinsBtnPressed(_ sender: Any) {
        if GADRewardBasedVideoAd.sharedInstance().isReady == true {
            GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
        }  else {
            print("Ad wasn't ready")
        }
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward) {
        score += Int(truncating: reward.amount)
        coinsLbl.text = "\(score)"
        Score.instance.saveScore(score: score)
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        hintBtns[0].isEnabled = true
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
        loadAndRequestRewardedVideo()
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        loadAndRequestRewardedVideo()
    }
    
    func loadAndRequestRewardedVideo() {
        hintBtns[0].isEnabled = false
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        let request = GADRequest()
        request.testDevices = ["b5b36ccd53370e45f0f66a6aa0e0c447", "c2fc9ae806b3e1b2a539dbffb5b85f4b"]
        GADRewardBasedVideoAd.sharedInstance().load(request, withAdUnitID: "ca-app-pub-2220353646614349/8204139299")
    }
    
    @IBAction func cleanBtnPressed(_ sender: Any) {
        let titleBtns = firstLineTitleBtns + secondLineTitleBtns
        var x = 0
        for t in titleBtns {
            if t.titleLabel?.text == "" || t.titleLabel?.text == nil || t.isEnabled == false {
                x += 1
            }
        }
        guard x != titleBtns.count else { return }
        Sounds.instance.btnSound?.play()
        for t in titleBtns {
            if t.isEnabled == true {
                t.setTitle("", for: .normal)
                t.titleLabel?.text = ""
                for b in letterBtns {
                    if b.tag == t.tag {
                        b.isHidden = false
                    }
                }
                t.tag = 0
            }
        }
    }
    
    @IBAction func storeBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        Sounds.instance.backSound?.play()
        dismiss(animated: false, completion: nil)
    }
}
