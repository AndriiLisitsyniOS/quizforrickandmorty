//
//  MenuVC.swift
//  quizForRickAndMorty
//
//  Created by andrey on 7/1/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MenuVC: UIViewController {

    @IBOutlet weak var coinsLbl: UILabel!
    @IBOutlet weak var soundBtn: MenuBtn!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var shareBtn: MenuBtn!
    
    var soundOff = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        soundOff = UserDefaults.standard.bool(forKey: "sound")
        soundBtn.setTitle("SOUND \(soundOff ? "OFF" : "ON")", for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        coinsLbl.text = "\(Score.instance.getScore())"
    }
    
    @IBAction func rateUsBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
        UIApplication.shared.open(URL(string : "itms-apps://itunes.apple.com/app/id1410705663")!, options: [:], completionHandler: nil)
        UserDefaults.standard.set(true, forKey: "rated")
    }
    
    @IBAction func moreAppsBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
        UIApplication.shared.open(URL(string : "https://itunes.apple.com/ua/developer/waygem/id1357956575")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func soundBtnPressed(_ sender: Any) {
        soundOff = !soundOff
        soundBtn.setTitle("SOUND \(soundOff ? "OFF" : "ON")", for: .normal)
        UserDefaults.standard.set(soundOff, forKey: "sound")
        if UserDefaults.standard.bool(forKey: "sound") == false {
            Sound.enabled = true
            Sounds.instance.btnSound?.play()
        } else {
            Sound.enabled = false
        }
    }
    @IBAction func shareBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
        var numberOfAnsweredQuestions = 0
        if UserDefaults.standard.dictionary(forKey: "levelQuestionDictionary") != nil {
            let levelQuestionDictionary = UserDefaults.standard.dictionary(forKey: "levelQuestionDictionary")!
            for l in levelQuestionDictionary {
                numberOfAnsweredQuestions += l.value as! Int
            }
        }
        
        let text = "I answered \(numberOfAnsweredQuestions) \(numberOfAnsweredQuestions == 1 ? "question" : "questions") in Trivia Quiz for Rick and Morty\nhttps://itunes.apple.com/app/id1410705663"
        let textToShare = [ text ]
        let activityVC = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        present(activityVC, animated: true, completion: nil)
        if let popOver = activityVC.popoverPresentationController {
            popOver.sourceView = self.shareView
            popOver.sourceRect = CGRect(x: shareBtn.frame.width/2 - 100, y: 0, width: 200, height: 200)
        }
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func restartBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
        let alert = UIAlertController(title: "Restart Game", message:"Are you sure you want ro restart the game? All saved data will be lost.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .destructive) { _ in
            UserDefaults.standard.removeObject(forKey: "levelQuestionDictionary")
            UserDefaults.standard.removeObject(forKey: "openLevelsArray")
            Score.instance.saveScore(score: 50)
            self.coinsLbl.text = "\(Score.instance.getScore())"
            self.dismiss(animated: false, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "NO", style: .cancel) { _ in
        })
        self.present(alert, animated: true){}
    }
    
    @IBAction func storeBtnPressed(_ sender: Any) {
        Sounds.instance.btnSound?.play()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        Sounds.instance.backSound?.play()
        dismiss(animated: false, completion: nil)
    }
}
